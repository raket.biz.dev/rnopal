---
layout: post
title: Deployment
parent: Mobile
permalink: /deployment/
nav_order: 4
---

# Deployment

Before you deploy your app to the app store and in play store you have to create repository first
It does not matter what repository you choose but for this tutorials we are using `Github`

## How to put code in Github

Make sure you have a [Github](https://github.com/) account

After signing up with github create your first repository. There are 2 ways to create repository 
1. In the github console.
![github](/images/github.png)


2. Github CLI 
```shell
brew install gh
```

In CLI you have to use Gitbash or brew for mac OS

It is also note that you have to upload public ssh key to github so you can easily push your code to github everytime you have some changes. in Mac you all you have to do is create ssh-keygen

```shell
ssh-keygen -t rsa -C "youremail@domain.com" -f "githubpersonal"
```
it will generate a private key and public key similar to this
![ssh-key](/images/key-gen.png)

after that copy the public key and put it into the github ssh key 
```shell
Profile>>Settings>SSH and GPG keys
```
once there click new [SSH key](https://github.com/settings/keys)

After that you are ready to save your code to Github

for first time uploading the command are
```shell
echo "# myApp" >> README.md
git init
git add README.md
git commit -m "first commit"
git branch -M dev
git remote add origin git@github.com:raketbizdev/myApp.git
git push -u origin dev
```

For continues development repo

```shell
git status
git add .
git commit -m "your commit message"
git push
```

# Deploying to IOS

To deploy your app to apple App store you need to enroll to apple developer programm and pay 100usd/yr and it takes a day or more to get verified.

While you are waiting you need to update your `app.json` file
and update this 2
```json
"bundleIdentifier": "com.username.myapp",
"buildNumber": "1.0.0"
```
```json
    "ios": {
      "supportsTablet": true,
      "bundleIdentifier": "com.username.myapp",
      "buildNumber": "1.0.0"
    },
```

after the update go to the terminal and run the build command for IOS
```shell
expo build:ios
```

There are selection one for `archive` and `simulator`
for app store deployment select `archive`
next question is if you have access to your apple id select `yes` if you already have otherwise need to go back to apple developer and enroll

next question is your apple ID and password so enter your apple id

Next question is the `Apple distribution certificate`. Every Apple developer has apple cert but luckyly Expo will create that for you. so select `expo handle the process`

Next is `Apple push notification` similar to ADC select
`expo handle the process`

Next is the `Apple Provisioning Profile` Once again select `expo handle the process`

Then build and wait until its build and once build go to expo then download it and upload to apple


# Deploying to Google play store

Similar to IOS you need to update tye `app.json` file

```json
    "android": {
      "package": "com.username.yourapp",
      "versioCode": 1
      "adaptiveIcon": {
        "foregroundImage": "./assets/adaptive-icon.png",
        "backgroundColor": "#FFFFFF"
        
      }
    },
```
Once the update is done run the command in the terminal
```shell
expo build:android
```
then select the `apk` if you want to test first but if you want to upload to google play store select `app-bundle`

Then the next question is `Key Store` if you already have a keystore enter it otherwise let `expo handle the process`

Wait for the build.

Once done downoload your keystore and dont lose it otherwise you can update your app in the google playstore
```shell
expo fetch:android:keystore
```
it will show the keystore and you will use it once your ready upload your app to google play store.






