---
layout: post
title: Texts
grand_parent: Mobile
parent: Development
permalink: /text/
nav_order: 3
---

## Text

Is a react component that displays text. Text support styling and touch event handling. To creat text is easy

```javascript
<Text>Your Text here!</Text>
```
Text can also suppor nesting elements.

```javascript
import React, { useState } from "react";
import { Text, StyleSheet } from "react-native";

const TextInANest = () => {
  const [titleText, setTitleText] = useState("Bird's Nest");
  const bodyText = "This is not really a bird nest.";

  const onPressTitle = () => {
    setTitleText("Bird's Nest [pressed]");
  };

  return (
    <Text style={styles.baseText}>
      <Text style={styles.titleText} onPress={onPressTitle}>
        {titleText}
        {"\n"}
        {"\n"}
      </Text>
      <Text numberOfLines={5}>{bodyText}</Text>
    </Text>
  );
};

const styles = StyleSheet.create({
  baseText: {
    fontFamily: "Cochin"
  },
  titleText: {
    fontSize: 20,
    fontWeight: "bold"
  }
});

export default TextInANest;

```

To learn more about text check the react-native [documentation](https://reactnative.dev/docs/text)