---
layout: post
title: Alert
grand_parent: Mobile
parent: Development
permalink: /alert/
nav_order: 6
---

## Alert

React Native Alert is an API which is used to display an alert dialog with specified title and message. It uses an alert() method to prompt an alert dialog. This Alert dialog provides three different lists of buttons (combination of neutral, negative, and positive) to perform action.

There are 2 types of alert: `Alert` and `Prompt`
Though prompt only works as of now for `IOS` but theres a [package](https://www.npmjs.com/package/react-native-prompt-android) that works on android



To create an alert is easy

```javascript
import React from 'react'
import AlertExample from './alert_example.js'

const App = () => {
   return (
      <AlertExample />
   )
}
export default App
```

### Alert example
We will create a button for triggering the showAlert function.

```javascript
import React from 'react'
import { Alert, Text, TouchableOpacity, StyleSheet } from 'react-native'

const AlertExample = () => {
   const showAlert = () =>{
      Alert.alert(
         'You need to...'
      )
   }
   return (
      <TouchableOpacity onPress = {showAlert} style = {styles.button}>
         <Text>Alert</Text>
      </TouchableOpacity>
   )
}
export default AlertExample

const styles = StyleSheet.create ({
   button: {
      backgroundColor: '#4ba37b',
      width: 100,
      borderRadius: 50,
      alignItems: 'center',
      marginTop: 100
   }
})
```

