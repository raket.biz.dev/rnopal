---
layout: post
title: Navigation
grand_parent: Mobile
parent: Development
permalink: /navigation/
nav_order: 7
---

## Navigating Between Screens

Mobile apps are rarely made up of a single screen. Managing the presentation of, and transition between, multiple screens is typically handled by what is known as a navigator.

To implement react navigation you can use another library called [react navigation](https://reactnavigation.org/docs/getting-started/)

to start you need to add dependency

npm
```shell
npm install @react-navigation/native
```
yarn
```shell
yarn add @react-navigation/native
```

For Expo manage project project use

```shell
npx expo install react-native-screens react-native-safe-area-context
```
## Type of navigation
* Stack navigator
* Tab Navigator
* Drawer navigator

### Stack navigator
In a web browser, you can link to different pages using an anchor (<a>) tag. When the user clicks on a link, the URL is pushed to the browser history stack. When the user presses the back button, the browser pops the item from the top of the history stack, so the active page is now the previously visited page. React Native doesn't have a built-in idea of a global history stack like a web browser does -- this is where React Navigation enters the story.

To start stack navigator you need to install the dependency

```shell
npm install @react-navigation/native-stack
```
After installation

```javascript
import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

function HomeScreen() {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>
    </View>
  );
}

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
```