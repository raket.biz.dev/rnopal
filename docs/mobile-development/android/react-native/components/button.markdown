---
layout: post
title: Buttons
grand_parent: Mobile
parent: Development
permalink: /buttons/
nav_order: 5
---
## Button

A button are touchable elements used to interact with screen and perform operations

```javascript
<Button
         onPress = {handlePress}
         title = "Red button!"
         color = "red"
      />
```
### with simple button
``` javascript
import { Button } from 'react-native'

const App = () => {
   const handlePress = () => false
   return (
      <Button
         onPress = {handlePress}
         title = "Red button!"
         color = "red"
      />
   )
}
export default App
```

### With touchable opacity
```javascript
import React from 'react'
import { TouchableOpacity, StyleSheet, View, Text } from 'react-native'

const App = () => {
   return (
      <View style = {styles.container}>
         <TouchableOpacity>
            <Text style = {styles.text}>
               Button
            </Text>
         </TouchableOpacity>
      </View>
   )
}
export default App

const styles = StyleSheet.create ({
   container: {
      alignItems: 'center',
   },
   text: {
      borderWidth: 1,
      padding: 25,
      borderColor: 'black',
      backgroundColor: 'red'
   }
})
```

### Touchable Highlight
When a user presses the element, it will get darker and the underlying color will show through.
```javascript
import React from 'react'
import { View, TouchableHighlight, Text, StyleSheet } from 'react-native'

const App = (props) => {
   return (
      <View style = {styles.container}>
         <TouchableHighlight> # Touchable Highlight
            <Text style = {styles.text}>
               Button
            </Text>
         </TouchableHighlight>
      </View>
   )
}
export default App

const styles = StyleSheet.create ({
   container: {
      alignItems: 'center',
   },
   text: {
      borderWidth: 1,
      padding: 25,
      borderColor: 'black',
      backgroundColor: 'red'
   }
})
```
### Touchable Native Feedback
This will simulate ink animation when the element is pressed.

```javascript
import React from 'react'
import { View, TouchableNativeFeedback, Text, StyleSheet } from 'react-native'

const Home = (props) => {
   return (
      <View style = {styles.container}>
         <TouchableNativeFeedback>
            <Text style = {styles.text}>
               Button
            </Text>
         </TouchableNativeFeedback>
      </View>
   )
}
export default Home

const styles = StyleSheet.create ({
   container: {
      alignItems: 'center',
   },
   text: {
      borderWidth: 1,
      padding: 25,
      borderColor: 'black',
      backgroundColor: 'red'
   }
})
```
To learn more about buttons check the react [documentation](https://reactnative.dev/docs/button)