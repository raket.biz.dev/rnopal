---
layout: post
title: Views
grand_parent: Mobile
parent: Development
permalink: /views/
nav_order: 1
---
## Views Component

Anything you need to know about the component is in the react native [documentation](https://reactnative.dev/docs/components-and-apis)

**Views** - is the most basic and fundamental component for building UI elemenents its like a `div` in html its use is a container

```javascript
export default function App() {
  console.log("App Execute")
  return (
    <View style={styles.container}>
      <Text>Hello World</Text>
    </View>
  );
}
```

to use view element you need to call it from the `react-native` import elements.

```javascript
import { View } from 'react-native';
```