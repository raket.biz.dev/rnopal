---
layout: post
title: Stylesheet
grand_parent: Mobile
parent: Development
permalink: /stylesheet/
nav_order: 2
---
## Stylesheet Component

React Native StyleSheet is a way of styling an application using JavaScript code, the main function of react native StyleSheet is concerned with styling and structuring of components in an application, all components of react native make use of a prop known as style, names and properties values work in a similar way as the web StyleSheet.

To create stylesheet. First you have to call the property or component by:

```javascript
import { StyleSheet } from 'react-native';
```

after calling it you can call a constant by creating:

```javascript
const styles = StyleSheet.create({
  container: {  # style ready to be called!
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
```

After defining it in the constant you can add it into your 

element by calling it

```javascript
export default function App() {
  return (
    <View style={styles.container}> # Where you call your styles
      <Text>Hello Floors</Text>
      <StatusBar style="auto" />
    </View>
  );
}
```

You can also do an inline object style by defining directly into your element.

```javascript
    <View style={\{backgrounColor: "orange"}}> # inline styles
      <Text>Hello Floors</Text>
      <StatusBar style="auto" />
    </View>
```

For external styles all you need to do is create a file that will contain all your styles

Eg:
```shell
touch myMainStyle.js
```
after that you have to call the react import.

```javascript
import { StyleSheet } from 'react-native';
```

once called start creating your stylesheet

```javascript
export default {
  container: {
    marginTop: 50,
  },
  bigBlue: {
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 30,
  },
  red: {
    color: 'red',
  },
};
```

to called it in the other files use the import method.

```javascript

import myMainStyle from './path/of/your/styles.js'
```

to use it into your current files

```javascript
    <View style={myMainStyle.yourObject}> # your external file.
      <Text>Hello Floors</Text>
      <StatusBar style="auto" />
    </View>
```
