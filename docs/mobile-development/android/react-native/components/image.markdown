---
layout: post
title: Images
grand_parent: Mobile
parent: Development
permalink: /images/
nav_order: 4
---

## Image

React Native provides a unified way of managing images and other media assets in your Android and iOS apps. To add a static image to your app, place it somewhere in your source code tree and reference it like this:

```javascript
<Image source={require('./file.png')} />
```

To render an image similar to other component you need to call import method

```javascript
import { Image } from 'react-native';
```

Onece called you can safely add it to you functions

```javascript
export default function App() {
  return (
    <View style={styles.container}>
      <Text style={myMainStyle.bigBlue}> Hello Floor</Text>
      <Image source={require('./your/file/path/file.png')} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
```

To render Network images

```javascript
<Image source={
  {
    width: 300,
    height: 200,
    uri: "https://picsum.photos/id/1/200/300"
  }
} />
```

The `require` syntax described above can be used to statically include audio, video or document files in your project as well. Most common file types are supported including `.mp3`, `.wav`, `.mp4`, `.mov`, `.html` and `.pdf`. See bundler defaults for the full list.

To learn more about image check the react [documentation](https://reactnative.dev/docs/images)