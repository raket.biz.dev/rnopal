---
layout: page
title: Android
parent: Mobile Development
has_children: true
permalink: /android/
nav_order: 3
---

# React Native Development

Welcome to react native mobile development.

## Preparation

In order to create a mobile app you need to install Visual studio code, Android studio and xcode if you want to build both IOS and Android.

- [Visual studio code](https://code.visualstudio.com/){:target="\_blank"}
- [Android Studio](https://developer.android.com/studio){:target="\_blank"}
- [Xcode](https://apps.apple.com/ph/app/xcode/id497799835?mt=12){:target="\_blank"}

see [tools](http://localhost:4000/building-mobile-app/#tools-for-building-mobile-app){:target="\_blank"} for building mobile app.

## Prerequisite

After all the preparation time for you to learn mobile development, but before we start we need to additionally install all the dependency to start coding.

- [NodeJs](https://nodejs.org/en/){:target="\_blank"} - You need the to install `NodeJs` in order for you to create a react native mobile application without it you cant run any javascript application as this is the server environment for all the javascript framework not just for `React Native` but also other framework such as `VueJS`, `Angular` and more...
- [React Native](https://reactnative.dev/){:target="\_blank"} - React native is what we are using to build a native mobile app React Native is just one way to build android app you can also use java or the java framework like kotlin, and for the IOS equivalent `Swift` and objective C.
- [Expo](https://expo.dev/){:target="\_blank"} - Expo is a bundle of tools created around React Native to help you start an app very fast. It provides you with a list of tools that simplify the creation and testing of React Native app. It equips you with the components of the user interface and services.
- [CocoaPods](https://cocoapods.org/){:target="\_blank"} - CocoaPods is a dependency manager for Swift and Objective-C Cocoa projects.

### Setting up the development environment.

After installing node go to your terminal and check if its working properly by typing

```
node -v
```

the output should be something like this.

```
v16.17.0
```

Once node is properly installed next to be installed is the expo cli

```
sudo npm i -g expo-cli
```

it is also highly recommended to install expo into your mobile app for debugging it is called `expo go` but im not gonna cover on this tutorials.

it takes some time to install the expo

Next is to install extensions in your visual studio code

![react native tools](/images/react-native-tools.png)
Another extensions is react native/React/redux/snippets for bunch of shortcut command
![react snippets](/images/react-snippets.png)
Next is the Prettier for code formatter
![prettier](/images/prettier.png)

To set your formatter you need to go to you the settings >> prefferences>> Settings then find `formatonsave` and then make sure to select the box so everytime you save your code it will be properly formatted.
![prettier](/images/format-on-save.png)

Another extension that you might need for development is material icons themes if you are using material icon for your app.
![prettier](/images/material-icon.png)

## Getting started

There are 3 ways to start a project `npm`, `yarn` and `expo`. In this Tutorials we are using expo

npm

```
$ npx react-native init myApp
```

npm

```
$ yarn create expo-app myApp
```

Expo

```
expo init myApp
```

after creating you app

you go to you application by going to to directory or open the visual studio code

```shell
- cd helloApp
- yarn start # you can open iOS, Android, or web from here, or run them directly with the commands below.
- yarn android
- yarn ios
- yarn web
```

After creating your app make sure you tested it on both phone simulator
![prettier](/images/assets-app.png)

```shell
› Metro waiting on exp://192.168.254.100:19000
› Scan the QR code above with Expo Go (Android) or the Camera app (iOS)

› Press a │ open Android
› Press i │ open iOS simulator
› Press w │ open web

› Press r │ reload app
› Press m │ toggle menu

› Press ? │ show all commands

Logs for your project will appear below. Press Ctrl+C to exit.
› Opening on iOS...
› Opening exp://192.168.254.100:19000 on iPhone 14
› Press ? │ show all commands
› Opening on Android...
› Opening exp://192.168.254.100:19000 on pixel
› Press ? │ show all commands
```

![phone ready](/images/emulator.png)

### Quick overview of the App

Asset folder is where you put all your assets elements

We also have the `App.js`

```javascript
import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";

export default function App() {
  return (
    <View style={styles.container}>
      <Text>Open up App.js to start working on your app!</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
```
