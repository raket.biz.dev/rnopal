---
layout: post
title: Building Mobile App What you need Know.
parent: Mobile
permalink: /building-mobile-app/
nav_order: 1
---

# Building mobile app what you need know?

## What is mobile app?
A mobile app is a software application developed specifically for use on small, wireless computing devices, such as smartphones and tablets, rather than desktop or laptop computers.

## When to build a mobile app?
Mobile apps are best used when building a service tailored around the mobile experience such as: On-demand services. Like Grab Lalamove or communication apps.

## What are types of mobile app.
* **Native** - Native mobile apps are built for a specific platform, such as iOS for the Apple iPhone or Android for a Samsung device. They are downloaded and installed via an app store and have access to system resources, such as GPS and the camera function. Mobile apps live and run on the device itself.
* **Hybrid** - Hybrid apps are essentially web apps that have been put in a native app shell. Once they are downloaded from an app store and installed locally, the shell is able to connect to whatever capabilities the mobile platform provides through a browser that's embedded in the app. one of the example is Twitter and Gmail.

* **PWA** - A progressive web application (PWA), commonly known as a progressive web app, is a type of application software delivered through the web they are not downloadable from playstore although now google play store does allow to upload PWA with just manifest.js.

## Tools for building mobile app
* **Android studio** - IDE and emulator. For installation check their [website](https://developer.android.com/studio#downloads)
* **Xcode** - For building IOS app and IOS emulator this only works on mac so make sure you have mac.
* **Visual Studio Code** - Most common IDE for developers not just for the Mobile developers but for other programming as well.
* **Expo Go** - [Expo Go/Expo Client](https://expo.dev/client) provides a preset SDK with commonly used APIs like basic view components, images, camera access, notifications, device info, and more. The preset SDK meets the needs of many professional apps that use today's managed workflow.
![expo](/images/expo.png "Developer companion")
