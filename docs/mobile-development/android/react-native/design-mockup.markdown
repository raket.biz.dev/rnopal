---
layout: post
title: Designing mockup
parent: Mobile
permalink: /design-mockup/
nav_order: 2
---

# What is a Design Mockup

A mockup is a static design of a web page or mobile application that features many of its final design elements but is not functional. A mockup is not as polished as a live page and typically includes some placeholder data. It's useful to breakdown each part of that definition.
![mockup](/images/mockup.png)

## BRD (Business Requirement Document)
A business requirements document describes the business solution for a project (i.e., what a new or updated product should do), including the user's needs and expectations, the purpose behind this solution, and any high-level constraints that could impact a successful deployment.


Never create a projects without BRD!
{: .warning }
### Why need Business Requirement Document
Business Requirement Documents (BRD) are usually created to gather all business requirements necessary to build a new application or replace a legacy business application. The BRDs are also drafted for a Request for Proposal (RFP) for a new project.

## Mindmap
Mind mapping is best known as a brainstorming exercise. You start with a central topic—surrounded by a bubble—then expand your ideas by adding additional bubbles that are each connected together with lines to create relationships. But mind mapping is useful for much more than just brainstorming.
![mockup](/images/mindmap.png)
## Userflow
User flow is the path taken by a prototypical user on a website or app to complete a task. The user flow takes them from their entry point through a set of steps towards a successful outcome and final action, such as purchasing a product.
![flowchart](/images/flowchart.jpg)

### Difference between Mindmap vs Userflow
Mindmapping is use for brainstorming on what is your app about and how to structure it. Userflow how your user interact with your app this happen after you build your mindmap.


## Prototype
What Is a Mobile App Prototype? A prototype is an interactive mockup of a mobile app. It contains key user interfaces, screens, and simulated functions without any working code or final design elements. Compared to a static wireframe or mockup, you can use a prototype just like any other app.
![flowchart](/images/prototype.gif)






