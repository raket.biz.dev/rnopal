---
layout: default
title: Module 1
grand_parent: Web Development
parent: Wordpress
nav_order: 1
---

# Module 1
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Create Your Own Theme

Creating a WordPress theme involves creating a set of template files and stylesheets that determine the layout and design of your website. The process can be broken down into the following steps:

1. Familiarize yourself with the WordPress template hierarchy, which determines the order in which template files are used to display different types of content on your website.

2. Create a new folder in the `wp-content/themes` directory of your WordPress installation and give it a unique name.

3. Create a `style.css` file in your new theme folder, which will be used to define the styles for your website.

4. Create a `functions.php` file in your new theme folder, which will be used to add functionality to your website.

5. Create template files such as `header.php`, `footer.php`, `index.php`, and `single.php` in your new theme folder, which will be used to display the different parts of your website.

6. Use HTML, CSS, and PHP to code the design and layout of your website in these template files.

7. Test your theme by activating it in the WordPress dashboard.

8. Customize and refine your theme as needed.

It's recommended to use a starter theme like [underscores](https://underscores.me/) or `blank` theme to start with, rather than starting from scratch. This will give you a basic set of templates and functions that you can modify to create your own theme.

### List of basic template files

- **header.php:** controls the header section, which includes the site title, logo, main navigation menu, and other elements.
- **footer.php:** controls the footer section, which includes site credits, secondary navigation menu, and other elements.
- **index.php:** controls the main blog index page, which displays a list of the latest posts.
- **single.php:** controls the display of individual blog posts.
- **page.php:** controls the display of individual pages.
- **archive.php:** controls the display of archive pages such as category, tag, author, and date archives.
- **search.php:** controls the display of search results.
- **404.php:** controls the display of 404 error page.
- **functions.php:** controls the functionality of your theme, where you can add new features or modify existing ones.
- **sidebar.php:** controls the display of sidebar on the theme
- **comments.php:** controls the display of comments section on the theme
- **category.php:** controls the display of category archive pages.
- **tag.php:** controls the display of tag archive pages
- **author.php:** controls the display of author archive pages
- **single-{post-type}.php:** controls the display of custom post type templates.
- **style.css:** This is the main stylesheet file that contains the CSS code that controls the visual appearance of the theme. It includes all the styling rules for the layout, typography, colors, and other visual elements of the theme. It is important to include the Theme meta-information at the top of this file, which is used by WordPress to identify the theme.
- **main.js:** This file contains the JavaScript code that controls the interactivity and behavior of the theme. It is used to add features like dropdown menus, image sliders, and other interactive elements to the theme.

## Built-in Theme Components


## Project Themes vs Child Themes


## Other Theme Enhancements


## Template Hierarchy


Hello

## Custom Post Types & Taxonomies


Hello

## Conditional Template Design