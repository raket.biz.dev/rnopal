---
layout: page
title: About
nav_order: 1
permalink: /about/
---

# About me

<section>
  <div class="card mb-3">
    <div class="row g-0">
      <div class="col-md-4">
        <img src="/images/ruelnopal.jpg" class="img-fluid rounded-start" alt="...">
      </div>
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title">Ruel Nopal</h5>
          <p class="card-text">Head of Engineering and Security Prosperna</p>
         Hi there! My name is Ruel and I am a self-taught programmer with 13 years of experience in the field of IT. Throughout my career, I have gained expertise in a range of areas including leadership, DevOps, and security.

I have a passion for using technology to solve problems and improve processes, which has led me to specialize in DevOps. I enjoy collaborating with development and operations teams to automate and streamline the software delivery process, and have a track record of successfully implementing and maintaining efficient IT systems.

In addition to my technical skills, I have also developed strong leadership abilities, having managed teams and projects throughout my career. I am a strong communicator and enjoy guiding and mentoring others to help them reach their full potential.

Security is another area of expertise for me, and I have a deep understanding of the importance of protecting sensitive data and systems from external threats. I am constantly learning and staying up-to-date on the latest security best practices to ensure that I can provide the highest level of protection for my clients and colleagues.

I am excited to use my skills and experience to continue learning and growing in my career, and to make a positive impact in the field of IT. If you have any questions or would like to learn more about my background and experience, please don't hesitate to reach out.

</div>
</div>
</div>

  </div>
</section>
